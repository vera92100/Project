const express = require("express");
const request = require("request");
const app = express();
const hbs = require("hbs");
hbs.registerPartials(__dirname + "/views/partials");

app.use("/css", express.static(__dirname + "/node_modules/bootstrap/dist/css"));

app.set("view engine", "hbs");

app.get("/", function (req, res) {
  res.render("index.hbs");
});

app.get("/weather", function (req, res) {
  console.log(req);
  if (req.query.city) {
    let link = generateLink(req.query.city);
    request(link, function (error, response, body) {
      console.log("City:" + link);
      if (!error && response.statusCode == 200) {
        let json = JSON.parse(body);

        let weather = {
          city: json.name,
          temp: json.main.temp,
          feels: json.main.feels_like,
        };
        res.render("weather.hbs", { weather });
      }
    });
  } else {
    res.render("weather.hbs");
  }
});

app.get("/weather/:city", function (req, res) {
  let link = generateLink(req.params.city);
  request(link, function (error, response, body) {
    console.log("City:" + link);
    if (!error && response.statusCode == 200) {
      let json = JSON.parse(body);

      let weather = {
        city: json.name,
        temp: json.main.temp,
        feels: json.main.feels_like,
      };
      res.render("weather.hbs", { weather });
    }
  });
});

app.listen("3991", () => {
  console.log("http://localhost:3991/");
});

const generateLink = (city) => {
  let key = "e89024a6ea1ca62e8db7c739d61301c9";
  let defaultCity = "Las Vegas";
  let link = `http://api.openweathermap.org/data/2.5/weather?q=${
    city ? city : defaultCity
  }&appid=${key}&units=metric`;
  console.log("City:" + city);
  return link;
};
